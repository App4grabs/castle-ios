//
//  NewsletterViewController.swift
//  Castle
//
//  Created by Gabriel Archer on 01/05/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit

class NewsletterViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!

    var refreshControl = UIRefreshControl()
    var isViewed = false
    var newsletters = Array<Newsletter>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing Posts")
        refreshControl.addTarget(self, action:#selector(NewsletterViewController.refresh(_:)), forControlEvents: .ValueChanged)
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        readLetters()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if isViewed {
            return
        }
        isViewed = true
        refreshControl.beginRefreshing()
        tableView.setContentOffset(CGPointMake(0, -self.refreshControl.frame.height), animated: false)
        loadLetters()
    }
    
    func refresh(sender: UIRefreshControl) {
        loadLetters()
    }
    
    func readLetters() {
        self.newsletters = DataManager.shared.getNewsletters(20)
        self.tableView.reloadData()
    }
    
    func loadLetters() {
        refreshControl.beginRefreshing()
        APIManager.shared.getNewsletters { (newsletters) in
            if newsletters != nil {
                DataManager.shared.saveNewsletters(newsletters!)
                self.refreshControl.endRefreshing()
                self.readLetters()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsletters.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        cell.textLabel?.text = newsletters[indexPath.row].subject
        cell.detailTextLabel?.text = NSDate(timestamp: newsletters[indexPath.row].time).shortDate()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let webViewController = storyboard?.instantiateViewControllerWithIdentifier("WebViewController") as? WebViewController {
            let lines = newsletters[indexPath.row].message.split("\n")
            var message = ""
            for line in lines {
                if !line.lowercaseString.contains("unsubscribe") {
                    message += "\(line)\n"
                }
            }
            webViewController.content = message
            webViewController.title = newsletters[indexPath.row].subject
            navigationController?.pushViewController(webViewController, animated: true)
        }
    }
}
