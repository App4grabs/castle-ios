//
//  APIManager.swift
//  Castle
//
//  Created by Gabriel Archer on 01/05/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import Alamofire

class APIManager: NSObject {
    
    static var shared = APIManager()

    func getNewsletters(completion: (newsletters: Array<Dictionary<String, AnyObject>>?) -> Void) {
        
        let data = [ "app": "castle" ]

        sendRequest(.GET, url: Config.shared.APIUrl + "apps/newsletter", data: data, alertTitle: "Newsletters", isIndicator: false) { (response) -> Void in
            if (response != nil) {
                completion(newsletters: response!["letters"] as? Array<Dictionary<String, AnyObject>>)
            } else {
                completion(newsletters: nil)
            }
        }
    }
    
    func sendRequest(
        method: Alamofire.Method,
        url: String,
        data: Dictionary<String, AnyObject>?,
        alertTitle: String,
        isIndicator: Bool,
        completion: (response: Dictionary<String, AnyObject>?) -> Void
        ) {
        var encoding = ParameterEncoding.JSON
        if method == .GET {
            encoding = ParameterEncoding.URLEncodedInURL
        }
        Alamofire.request(method, url, parameters: data, encoding: encoding, headers: ["Content-Type": "application/json"])
            .responseJSON { (response) -> Void in
                if (response.result.error == nil) {
                    do {
                        if let answer = try NSJSONSerialization.JSONObjectWithData(response.data!, options: .MutableContainers) as? Dictionary<String, NSObject> {
                            if answer["errorMessage"] != nil {
                                let message = answer["errorMessage"] as! String
                                self.alert(alertTitle, message: message)
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    completion(response: nil)
                                })
                            } else if answer["message"] == "Missing Authentication Token" {
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.alert(alertTitle, message: "No endpoint")
                                    completion(response: nil)
                                })
                            } else {
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    completion(response: answer)
                                })
                            }
                        }
                        
                    } catch let error as NSError {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.alert(alertTitle, message: error.localizedDescription)
                            completion(response: nil)
                        })
                    }
                } else {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.alert(alertTitle, message: response.result.error!.localizedDescription)
                        completion(response: nil)
                    })
                }
        }
    }
    
    func alert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
    }
    

    
}

