//
//  CommentsViewController.swift
//  Castle
//
//  Created by Gabriel Archer on 05/05/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import Kingfisher

class CommentsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var InputBottom: NSLayoutConstraint!
    @IBOutlet weak var inputField: UITextField!
    
    var refreshControl = UIRefreshControl()
    var facebookPost: FacebookPost?
    var comments = Array<FacebookComment>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let menuButton = UIButton(frame: CGRectMake(0, 0, 48, 40))
        menuButton.setImage(UIImage(named: "back"), forState: .Normal)
        menuButton.contentHorizontalAlignment = .Left
        menuButton.addTarget(self, action: #selector(WebViewController.back), forControlEvents: .TouchUpInside)
        let leftButton = UIBarButtonItem(customView: menuButton)
        navigationItem.leftBarButtonItem = leftButton
        navigationItem.hidesBackButton = true

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing Posts")
        refreshControl.addTarget(self, action:#selector(PostsViewController.refresh(_:)), forControlEvents: .ValueChanged)
        tableView.addSubview(refreshControl)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        title = facebookPost!.message

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CommentsViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CommentsViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        readComments()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        inputField.becomeFirstResponder()
        refreshControl.beginRefreshing()
        tableView.setContentOffset(CGPointMake(0, -self.refreshControl.frame.height), animated: false)
        loadComments()
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size {
            if let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSTimeInterval {
                UIView.animateWithDuration(duration, animations: { 
                    self.InputBottom.constant = keyboardSize.height
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as? NSTimeInterval {
            UIView.animateWithDuration(duration, animations: {
                self.InputBottom.constant = 40
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func back() {
        navigationController?.popViewControllerAnimated(true)
    }

    func readComments() {
        comments = DataManager.shared.getFacebookComments(facebookPost!.postId)
        tableView.reloadData()
    }
    
    func loadComments() {
        refreshControl.beginRefreshing()
        FacebookManager.shared.getAppToken { (success) -> Void in
            if success {
                FacebookManager.shared.getComments(self.facebookPost!.postId) { (result) -> Void in
                    if let comments = result?["data"] as? Array<Dictionary<String, AnyObject>>  {
                        DataManager.shared.saveFacebookComments(comments, postId: self.facebookPost!.postId)
                        self.readComments()
                    }
                }
            } else {
                print("No Token")
            }
        }
        self.readComments()
        refreshControl.endRefreshing()
    }
    
    func refresh(sender: UIRefreshControl) {
        loadComments()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - User interactions
    
    @IBAction func editingStarted(sender: UITextField) {
        print("started")
    }
    
    @IBAction func closeKeyboard(sender: UITextField) {
        inputField.resignFirstResponder()
    }
    
    @IBAction func sendComment(sender: AnyObject) {
        if inputField.text!.isEmpty {
            return
        }
        FacebookManager.shared.canRefreshToken(self, todo: "comment") { (success) in
            if success {
                FacebookManager.shared.sendComment(self.facebookPost!.postId, comment: self.inputField.text!) { (success) in
                    if success {
                        self.inputField.text = "";
                        self.inputField.resignFirstResponder()
                        self.loadComments()
                    } else {
                        APIManager.shared.alert("Comment", message: "There is an error in the communication.")
                    }
                }
            }
        }
        
        
    }
    
    //MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! CommentsCell

        let comment = comments[indexPath.row]
        cell.messageLabel.text = comment.message
        
        if (!comment.createdTime.isEmpty) {
            let date = NSDate(dateString: comment.createdTime)
            cell.dateLabel.text = date.shortDateTime()
        }
        
        if let url = NSURL(string: comment.fromPicture) {
            cell.fromPictureView.kf_setImageWithURL(url, placeholderImage: UIImage(named: "noprofile"), optionsInfo: [.Transition(ImageTransition.Fade(1))])
        }
        cell.fromPictureView.layer.cornerRadius = 20
        cell.fromPictureView.layer.masksToBounds = true
        
        cell.fromNameLabel.text = comment.fromName

        return cell
    }
}

class CommentsCell: UITableViewCell {
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var fromPictureView: UIImageView!
    @IBOutlet weak var fromNameLabel: UILabel!
    
}
