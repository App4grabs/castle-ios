//
//  BaseViewController.swift
//  Castle
//
//  Created by Gabriel Archer on 06/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import SWRevealViewController

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil {
            let menuButton = UIButton(frame: CGRectMake(0, 0, 48, 40))
            menuButton.setImage(UIImage(named: "burger"), forState: .Normal)
            menuButton.contentHorizontalAlignment = .Left
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: .TouchUpInside)
            let leftButton = UIBarButtonItem(customView: menuButton)
            navigationItem.leftBarButtonItem = leftButton
            navigationItem.hidesBackButton = true
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if NSUserDefaults.standardUserDefaults().boolForKey("Newsletter") {
            if let newsletterNav = storyboard?.instantiateViewControllerWithIdentifier("NewsletterNav") as? UINavigationController {
                revealViewController().setFrontViewController(newsletterNav, animated: true)
            }
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "Newsletter")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
