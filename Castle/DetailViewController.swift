//
//  DetailViewController.swift
//  Castle
//
//  Created by Gabriel Archer on 06/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import RTLabel

class DetailViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let titles = ["Course length:", "Hours per Week:", "Lessons:", "Timetable:", "CEF Levels:", ""]
    var course: Dictionary<String, String>?
    var headers = 6
    var details = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        let menuButton = UIButton(frame: CGRectMake(0, 0, 48, 40))
        menuButton.setImage(UIImage(named: "back"), forState: .Normal)
        menuButton.contentHorizontalAlignment = .Left
        menuButton.addTarget(self, action: #selector(DetailViewController.back), forControlEvents: .TouchUpInside)
        let leftButton = UIBarButtonItem(customView: menuButton)
        navigationItem.leftBarButtonItem = leftButton
        navigationItem.hidesBackButton = true
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 56
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if course != nil {
            title = course?["title"] ?? ""
        }
        let warning = course?["warning"] ?? ""
        if warning.isEmpty {
            headers = 5
        }

        if let include = course?["include"] {
            if let path = NSBundle.mainBundle().pathForResource(include, ofType: "html") {
                self.details = (try? String(contentsOfFile: path)) ?? ""
            }
        }
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func back() {
        navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headers + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row < headers {
            let cell = tableView.dequeueReusableCellWithIdentifier("HeaderCell", forIndexPath: indexPath) as! DetailHeaderCell
            cell.titleLabel.text = titles[indexPath.row]
            cell.topConstraint.constant = 15
            switch indexPath.row {
            case 0:
                cell.detailLabel.text = course?["length"]
                break
                
            case 1:
                cell.detailLabel.text = course?["hours"]
                break
                
            case 2:
                cell.detailLabel.text = course?["lessons"]
                break
                
            case 3:
                cell.detailLabel.text = course?["timetable"]
                break
                
            case 4:
                cell.detailLabel.text = (course?["cef"] ?? "") + "\n" + (course?["levels"] ?? "")
                break
                
            case 5:
                cell.detailLabel.text = course?["warning"]
                cell.topConstraint.constant = 0
                break
                
            default:
                break
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! DetailCell
            cell.rtLabel.text = details
            cell.rtLabel.font = UIFont.systemFontOfSize(15)
            cell.rtLabel.textColor = UIColor.blackColor()
            let optimumSize = cell.rtLabel.optimumSize
            cell.heightConstraint.constant = optimumSize.height
            
            return cell
        }
    }
}

class DetailHeaderCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
}

class DetailCell: UITableViewCell {
    @IBOutlet weak var rtLabel: RTLabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
}
