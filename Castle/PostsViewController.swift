//
//  PostsViewController.swift
//  Castle
//
//  Created by Gabriel Archer on 11/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKShareKit
import Kingfisher

class PostsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    let accessToken = ""
    var refreshControl = UIRefreshControl()
    var facebookPosts = Array<FacebookPost>()
    var isViewed = false

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing Posts")
        refreshControl.addTarget(self, action:#selector(PostsViewController.refresh(_:)), forControlEvents: .ValueChanged)
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        readPosts()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if !isViewed {
            refreshControl.beginRefreshing()
            tableView.setContentOffset(CGPointMake(0, -self.refreshControl.frame.height), animated: false)
            isViewed = true
        }
        loadPosts()
    }
    
    func readPosts() {
        self.facebookPosts = DataManager.shared.getFacebookPosts(50)
        self.tableView.reloadData()
    }
    
    func loadPosts() {
        refreshControl.beginRefreshing()
        FacebookManager.shared.getAppToken { (success) -> Void in
            if success {
                FacebookManager.shared.getFeed("135762093160064") { (result) -> Void in
                    if let posts = result?["data"] as? Array<Dictionary<String, AnyObject>>  {
                        DataManager.shared.saveFacebookPosts(posts)
                        self.readPosts()
                        self.refreshControl.endRefreshing()
                    }
                }
            } else {
                self.refreshControl.endRefreshing()
                print("No Token")
            }
        }
        self.readPosts()
    }

    func refresh(sender: UIRefreshControl) {
        loadPosts()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func moreButtonTouched(sender: UIButton) {
        let link = facebookPosts[sender.tag].link
        if let url = NSURL(string: link) {
            if let viewController = storyboard?.instantiateViewControllerWithIdentifier("WebViewController") as? WebViewController {
                viewController.url = url
                viewController.delegate = nil
                viewController.title = "Content"
                navigationController?.pushViewController(viewController, animated: true)
            }
            
        }
    }
    
    @IBAction func sharePost(sender: UIButton) {
        let facebookPost = facebookPosts[sender.tag]
        let shareController = UIActivityViewController(activityItems: [facebookPost.link], applicationActivities: nil)
        presentViewController(shareController, animated: true, completion: nil)
    }
    
    @IBAction func likePost(sender: UIButton) {
        let facebookPost = facebookPosts[sender.tag]
        FacebookManager.shared.canRefreshToken(self, todo: "like a post") { (success) in
            if success {
                FacebookManager.shared.like(facebookPost.postId, delete: sender.selected, completion: { (success) in
                    if success {
                        sender.selected = !sender.selected
                        DataManager.shared.likeFacebookPost(sender.selected, postId: facebookPost.postId)
                        self.readPosts()
                    } else {
                        APIManager.shared.alert("Like", message: "There is an error in the communication.")
                    }
                })
            }
        }
    }
    
    @IBAction func showComments(sender: UIButton) {
        if let commentsViewController = storyboard?.instantiateViewControllerWithIdentifier("CommentsViewController") as? CommentsViewController {
            commentsViewController.facebookPost = facebookPosts[sender.tag]
            navigationController?.pushViewController(commentsViewController, animated: true)
        }
    }
    // MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return facebookPosts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PostsCell
        
        let facebookPost = facebookPosts[indexPath.row]
        cell.messageLabel.text = facebookPost.message
        
        if (!facebookPost.createdTime.isEmpty) {
            let date = NSDate(dateString: facebookPost.createdTime)
            cell.dateLabel.text = date.shortDateTime()
        }
        
        if let url = NSURL(string: facebookPost.picture) {
            cell.pictureView.kf_setImageWithURL(url, placeholderImage: UIImage(named: "noimage"), optionsInfo: [.Transition(ImageTransition.Fade(1))])
        }
        cell.fromPictureView.layer.cornerRadius = 20
        cell.fromPictureView.layer.masksToBounds = true
        
        cell.fromNameLabel.text = facebookPost.fromName
        cell.moreButton.tag = indexPath.row
        cell.likeButton.tag = indexPath.row
        cell.commentsButton.tag = indexPath.row
        cell.pictureButton.tag = indexPath.row
        
        cell.youtubeView.hidden = true
        cell.pictureButton.setImage(nil, forState: .Normal)
        cell.pictureButton.hidden = false
        if facebookPost.type == "video" && facebookPost.link.contains("youtu") {
            cell.pictureButton.hidden = true
            cell.youtubeView.hidden = false
            cell.youtubeView.loadVideoURL(NSURL(string: facebookPost.link)!)
        } else if facebookPost.type == "video" {
            cell.pictureButton.setImage(UIImage(named: "play"), forState: .Normal)
        } else if facebookPost.type == "status" {
            cell.pictureButton.hidden = true
        }
        cell.likeButton.selected = facebookPost.liked
        cell.setActivity(facebookPost.liked, likes: facebookPost.likes, comments: facebookPost.comments, shares: facebookPost.shares)
        cell.setNeedsUpdateConstraints()
        
        return cell
    }

}

class PostsCell: UITableViewCell {
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var pictureButton: UIButton!
    @IBOutlet weak var youtubeView: YouTubePlayerView!
    @IBOutlet weak var fromPictureView: UIImageView!
    @IBOutlet weak var fromNameLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentsButton: UIButton!
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var sharesLabel: UILabel!
    @IBOutlet weak var commentsMargin: NSLayoutConstraint!
    @IBOutlet weak var activityHeight: NSLayoutConstraint!
    
    func setActivity(liked: Bool, likes: Int, comments: Int, shares: Int) {
        if comments + likes + shares == 0 {
            activityHeight.constant = 0
        } else {
            activityHeight.constant = 20
        }
        
        if likes == 0 {
            likesLabel.text = ""
        } else if likes == 1 {
            if liked {
                likesLabel.text = "You like this"
            } else {
                likesLabel.text = "1 person likes this"
            }
        } else if likes == 2 {
            if liked {
                likesLabel.text = "You and 1 person like this"
            } else {
                likesLabel.text = "2 people like this"
            }
        } else {
            if liked {
                likesLabel.text = "You and \(likes - 1) people like this"
            } else {
                likesLabel.text = "\(likes) people like this"
            }
        }
        
        if shares == 0 {
            sharesLabel.text = ""
            commentsMargin.constant = 0
        } else if shares == 1 {
            sharesLabel.text = "1 Share"
            commentsMargin.constant = 10
        } else {
            sharesLabel.text = "\(shares) Shares"
            commentsMargin.constant = 10
        }
        
        if comments == 0 {
            commentsLabel.text = ""
        } else if comments == 1 {
            commentsLabel.text = "1 Comment"
        } else {
            commentsLabel.text = "\(comments) Comments"
        }
    }
}
