//
//  DataManager.swift
//  Castle
//
//  Created by Gabriel Archer on 12/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import RealmSwift
import Kingfisher

class DataManager: NSObject {
    
    static var shared = DataManager()
    
    override init() {
        super.init()
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            schemaVersion: 6,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 6) {
                    if let path = Realm.Configuration.defaultConfiguration.path {
                        try! NSFileManager().removeItemAtPath(path)
                    }
                }
            }
        )
        _ = try! Realm()
    }
    
    func saveFacebookPosts(posts: Array<Dictionary<String, AnyObject>>) {
        let realm = try! Realm()
        for post in posts {
            print(post)
            if let postId = post["id"] as? String {
                if realm.objects(FacebookPost).filter("postId = '\(postId)'").count == 0 {
                    let facebookPost = FacebookPost()
                    facebookPost.postId =  postId
                    facebookPost.type = post["type"] as? String ?? ""
                    if facebookPost.type == "event" {
                        facebookPost.message = post["name"] as? String ?? ""
                    } else {
                        facebookPost.message = post["message"] as? String ?? ""
                    }
                    if facebookPost.type == "photo" && facebookPost.message.isEmpty {
                        facebookPost.message = post["story"] as? String ?? ""
                    }
                    facebookPost.link = post["link"] as? String ?? ""
                    facebookPost.createdTime = post["created_time"] as? String ?? ""
                    
                    // Picture
                    var picture = post["picture"] as? String ?? ""
                    var p = picture.indexOf("url=") + 4
                    if (p > 4) {
                        picture = picture.substringFrom(p)
                        p = picture.indexOf("&")
                        if (p > 1) {
                            picture = picture.substringTo(p)
                        }
                        picture = picture.urlDecode()
                    } else {
                        picture = post["full_picture"] as? String ?? ""
                    }
                    facebookPost.picture = picture
                    
                    //From
                    if let from = post["from"] as? Dictionary<String, String> {
                        facebookPost.fromId = from["id"] ?? ""
                        facebookPost.fromName = from["name"] ?? ""
                    }
                    if !facebookPost.fromId.isEmpty {
                        facebookPost.fromPicture = "https://graph.facebook.com/\(facebookPost.fromId)/picture/?type=normal"
                    }
                    
                    _ = try? realm.write({ () -> Void in
                        realm.add(facebookPost)
                    })
                }

                //Update activities
                var liked = false
                let userId = NSUserDefaults.standardUserDefaults().stringForKey("FBUserId")
                let facebookPost = realm.objects(FacebookPost).filter("postId = '\(postId)'").first
                if facebookPost != nil {
                    var likesCount = 0
                    var commentsCount = 0
                    var sharesCount = 0
                    if let likes = post["likes"] as? Dictionary<String, AnyObject> {
                        if let summary = likes["summary"] as? Dictionary<String, AnyObject> {
                            if let count = summary["total_count"] as? Int {
                                likesCount = count
                            }
                        }
                        if let data = likes["data"] as? Array<Dictionary<String, AnyObject>> {
                            for like in data {
                                if let uid = like["id"] as? String {
                                    if uid == userId {
                                        liked = true
                                    }
                                }
                            }
                        }

                    }
                    if let comments = post["comments"] as? Dictionary<String, AnyObject> {
                        if let summary = comments["summary"] as? Dictionary<String, AnyObject> {
                            if let count = summary["total_count"] as? Int {
                                commentsCount = count
                            }
                        }
                    }
                    if let shares = post["shares"] as? Dictionary<String, AnyObject> {
                        if let count = shares["count"] as? Int {
                            sharesCount = count
                        }
                    }

                    _ = try? realm.write({ () -> Void in
                        facebookPost!.likes = likesCount
                        facebookPost!.comments = commentsCount
                        facebookPost!.shares = sharesCount
                        facebookPost!.liked = liked
                    })
                }
                
            }
        }
    }
    
    func likeFacebookPost(liked: Bool, postId: String) {
        let realm = try! Realm()
        let facebookPost = realm.objects(FacebookPost).filter("postId = '\(postId)'").first
        if facebookPost != nil {
            var likes = facebookPost!.likes
            if liked {
                likes += 1
            } else {
                likes -= 1
                if likes < 0 {
                    likes = 0
                }
            }
            _ = try? realm.write({ () -> Void in
                facebookPost!.likes = likes
                facebookPost!.liked = liked
            })
        }
    }
    
    func getFacebookPosts(limit: Int) -> Array<FacebookPost> {
        var resultArray = Array<FacebookPost>()
        let realm = try! Realm()
        let results = realm.objects(FacebookPost).sorted("createdTime", ascending: false)
        var counter = 0;
        for facebookPost in results {
            if counter < limit {
                resultArray.append(facebookPost)
            }
            counter += 1
        }
        return resultArray
    }
    
    func saveFacebookComments(comments: Array<Dictionary<String, AnyObject>>, postId: String) {
        let realm = try! Realm()
        for comment in comments {
            print(comment)
            if let commentId = comment["id"] as? String {
                if realm.objects(FacebookComment).filter("commentId = '\(commentId)'").count == 0 {
                    let facebookComment = FacebookComment()
                    facebookComment.commentId =  commentId
                    facebookComment.postId =  postId
                    facebookComment.message =  comment["message"] as? String ?? ""
                    facebookComment.createdTime = comment["created_time"] as? String ?? ""
                    
                    //From
                    if let from = comment["from"] as? Dictionary<String, String> {
                        facebookComment.fromId = from["id"] ?? ""
                        facebookComment.fromName = from["name"] ?? ""
                    }
                    if !facebookComment.fromId.isEmpty {
                        facebookComment.fromPicture = "https://graph.facebook.com/\(facebookComment.fromId)/picture/?type=normal"
                    }
                    
                    
                    _ = try? realm.write({ () -> Void in
                        realm.add(facebookComment)
                    })
                }
                
            }
        }
    }

    func getFacebookComments(postId: String) -> Array<FacebookComment> {
        var resultArray = Array<FacebookComment>()
        let realm = try! Realm()
        let results = realm.objects(FacebookComment).filter("postId = '\(postId)'").sorted("createdTime", ascending: true)
        for facebookComment in results {
            resultArray.append(facebookComment)
        }
        return resultArray
    }
    
    func saveNewsletters(letters: Array<Dictionary<String, AnyObject>>) {
        let realm = try! Realm()
        for letter in letters {
            if let letterId = letter["id"] as? Int {
                if realm.objects(Newsletter).filter("letterId = \(letterId)").count == 0 {
                    let newsletter = Newsletter()
                    newsletter.letterId = letterId
                    newsletter.subject = letter["subject"] as? String ?? ""
                    newsletter.time = letter["time"] as? Int ?? 0
                    newsletter.message = letter["content"] as? String ?? ""
                    if newsletter.time != 0 && !newsletter.subject.isEmpty && !newsletter.message.isEmpty {
                        _ = try? realm.write({ () -> Void in
                            realm.add(newsletter)
                        })
                    }
                }
            }
        }

    }
        
    func getNewsletters(limit: Int) -> Array<Newsletter> {
        var resultArray = Array<Newsletter>()
        let realm = try! Realm()
        let results = realm.objects(Newsletter).sorted("time", ascending: false)
        var counter = 0
        for newsletter in results {
            if counter < limit {
                resultArray.append(newsletter)
                counter += 1
            }
        }
        return resultArray
    }
    
    func clearData() {
        let realm = try! Realm()
        _ = try? realm.write {
            realm.deleteAll()
        }
        let cache = KingfisherManager.sharedManager.cache
        cache.clearMemoryCache()
        cache.clearDiskCache()
        DataManager.shared = DataManager()
    }
    
}
