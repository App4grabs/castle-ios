//
//  ContactViewController.swift
//  Castle
//
//  Created by Gabriel Archer on 08/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import MessageUI

class ContactViewController: BaseViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var emailButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emailButton.layer.cornerRadius = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendMessage(sender: UIButton) {
        if let url = NSURL(string: "fb-messenger://user-thread/\(Config.shared.FacebookPageId)") {
            if UIApplication.sharedApplication().canOpenURL(url) {
                UIApplication.sharedApplication().openURL(url)
            } else {
                APIManager.shared.alert("Messenger", message: "Facebook Messenger is not installed")
            }
        }
    }
    
    @IBAction func sendEmail(sender: UIButton) {
        if !MFMailComposeViewController.canSendMail() {
            //alert("There is no account set to Mail app!")
            return
        }
        let msgPicker = MFMailComposeViewController()
        msgPicker.mailComposeDelegate = self
        msgPicker.setToRecipients(["contactus@castle-school.co.uk"])
        msgPicker.setSubject("Enquiry from Castle School app")
        msgPicker.setMessageBody("", isHTML: true)
        self.presentViewController(msgPicker, animated: true, completion: nil)
    }
    
    @IBAction func callPhone(sender: UIButton) {
        if let url = NSURL(string: "tel://+441273748185") {
            if UIApplication.sharedApplication().canOpenURL(url) {
                UIApplication.sharedApplication().openURL(url)
            } else {
                APIManager.shared.alert("Messenger", message: "You cannot phone on this device")
            }
        }
    }
    
    //MARK: - Mail composer delegate
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        if (error == nil) {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }


}
