//
//  FacebookComments.swift
//  Castle
//
//  Created by Gabriel Archer on 05/05/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import Foundation
import RealmSwift

class FacebookComment: Object {
    
    dynamic var postId = ""
    dynamic var commentId = ""
    dynamic var message = ""
    dynamic var createdTime = ""
    dynamic var fromId = ""
    dynamic var fromName = ""
    dynamic var fromPicture = ""
    
    override class func primaryKey() -> String {
        return "commentId"
    }
    
}
