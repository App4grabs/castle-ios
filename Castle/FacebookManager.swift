//
//  FacebookManager.swift
//  Castle
//
//  Created by Gabriel Archer on 11/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Alamofire
import Accounts
import Social

class FacebookManager: NSObject {
    
    let permissions = ["public_profile", "email", "user_friends"]
    
    static var shared = FacebookManager()
    
    var appAccessToken: String?
    var accountStore = ACAccountStore()
    var account: ACAccount?
    
    override init() {
        super.init()
    }
    
    func refreshToken(completion: ((success: Bool) -> Void)?) {
        if FBSDKAccessToken.currentAccessToken() != nil {
            if !FBSDKAccessToken.currentAccessToken().permissions.contains("publish_actions") {
                completion?(success: false)
                return
            }
            
            if FBSDKAccessToken.currentAccessToken().expirationDate.timeIntervalSince1970 < NSDate().timeIntervalSince1970 - 600 {
                FBSDKAccessToken.refreshCurrentAccessToken({ (connection, object, error) -> Void in
                    if error == nil {
                        completion?(success: true)
                    } else {
                        completion?(success: false)
                    }
                })
            } else {
                completion?(success: true)
            }
        } else {
            completion?(success: false)
        }
    }
    
    func canRefreshToken(viewController: UIViewController, todo: String, completion: ((success: Bool) -> Void)?) {
        refreshToken { (success) in
            if success {
                completion?(success: true)
            } else {
                let alertController = UIAlertController(title: "Facebook login required", message: "To \(todo), you have to log in to Facebook", preferredStyle: .Alert)
                let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: { (action) in
                    self.login(viewController, completion: { (success) in
                        completion?(success: success)
                    })
                })
                alertController.addAction(defaultAction)
                let cancelAction = UIAlertAction(title: "Not now", style: .Cancel, handler: nil)
                alertController.addAction(cancelAction)
                viewController.presentViewController(alertController, animated: true, completion: nil)
            }
        }
        
    }
    
    func login(viewController: UIViewController, completion: ((success: Bool) -> Void)?) {
        if FBSDKAccessToken.currentAccessToken() != nil {
            if !FBSDKAccessToken.currentAccessToken().permissions.contains("publish_actions") {
                self.loginForPublish(viewController, completion: { (success) in
                    completion?(success: success)
                    return
                })
            }
        }
        let loginManager = FBSDKLoginManager()
        loginManager.logInWithReadPermissions(["public_profile", "email", "user_friends"], fromViewController: viewController, handler: { (result, error) -> Void in
            if error != nil {
                print("Error")
                completion?(success: false)
            } else if result.isCancelled {
                completion?(success: false)
            } else {
                let userId = FBSDKAccessToken.currentAccessToken().userID
                NSUserDefaults.standardUserDefaults().setObject(userId, forKey: "FBUserId")
                NSUserDefaults.standardUserDefaults().synchronize()
                self.loginForPublish(viewController, completion: { (success) in
                    completion?(success: success)
                })
            }
        })

    }
    
    func loginForPublish(viewController: UIViewController, completion: ((success: Bool) -> Void)?) {
        let loginManager = FBSDKLoginManager()
        loginManager.logInWithPublishPermissions(["publish_actions"], fromViewController: viewController, handler: { (result, error) in
            if error != nil {
                print("Error")
                completion?(success: false)
            } else if result.isCancelled {
                completion?(success: FBSDKAccessToken.currentAccessToken().permissions.contains("publish_actions"))
            } else {
                completion?(success: true)
            }
        })
    }

    func getAppToken(completion: ((success: Bool) -> Void)?) {
        
        if appAccessToken != nil {
            completion?(success: true)
            return
        }
        FBSDKGraphRequest(
            graphPath: "oauth/access_token",
            parameters: [
                "client_id": Config.shared.FacebookAppId,
                "client_secret": Config.shared.FacebookAppSecret,
                "grant_type": "client_credentials"
            ],
            HTTPMethod: "GET"
            ).startWithCompletionHandler { (connection, result, error) -> Void in
            if error == nil {
                if let resultObject = result as? Dictionary<String, AnyObject> {
                    self.appAccessToken = resultObject["access_token"] as? String
                }
                completion?(success: true)
            } else {
                completion?(success: false)
            }
        }
    }

    func getFeed(pageName: String, completion: ((result: Dictionary<String, AnyObject>?) -> Void)?) {
        if appAccessToken == nil {
            completion?(result: nil)
            return
        }
        Alamofire.request(
            .GET,
            "https://graph.facebook.com/\(pageName)/posts",
            parameters: ["access_token": "\(Config.shared.FacebookAppId)|\(Config.shared.FacebookAppSecret)", "limit": 100, "fields": "type,name,message,story,link,picture,full_picture,from,created_time,likes.summary(true),comments.limit(1).summary(true),shares"],
            encoding: ParameterEncoding.URL,
            headers: nil
        ).responseJSON { (response) -> Void in
            if response.result.error == nil {
                completion?(result: response.result.value as? Dictionary<String, AnyObject>)
            } else {
                print(response.result.error?.debugDescription)
                completion?(result: nil)
            }
        }
    }
    
    func getComments(postId: String, completion: ((result: Dictionary<String, AnyObject>?) -> Void)?) {
        if appAccessToken == nil {
            completion?(result: nil)
            return
        }
        Alamofire.request(
            .GET,
            "https://graph.facebook.com/\(postId)/comments",
            parameters: ["access_token": "\(Config.shared.FacebookAppId)|\(Config.shared.FacebookAppSecret)"],
            encoding: ParameterEncoding.URL,
            headers: nil
            ).responseJSON { (response) -> Void in
                if response.result.error == nil {
                    completion?(result: response.result.value as? Dictionary<String, AnyObject>)
                } else {
                    print(response.result.error?.debugDescription)
                    completion?(result: nil)
                }
        }
        
    }
    
    func sendComment(postId: String, comment: String, completion: ((success: Bool) -> Void)?) {
        Alamofire.request(
            .POST,
            "https://graph.facebook.com/\(postId)/comments",
            parameters: [
                "access_token": FBSDKAccessToken.currentAccessToken().tokenString,
                "message": comment
            ],
            encoding: ParameterEncoding.URL,
            headers: nil
            ).responseJSON { (response) -> Void in
                if response.result.error == nil {
                    completion?(success: response.result.isSuccess)
                } else {
                    print(response.result.error?.debugDescription)
                    completion?(success: false)
                }
        }
    }

    func like(postId: String, delete: Bool, completion: ((success: Bool) -> Void)?) {
        var method = Alamofire.Method.POST
        if delete {
            method = Alamofire.Method.DELETE
        }
        Alamofire.request(
            method,
            "https://graph.facebook.com/\(postId)/likes",
            parameters: [
                "access_token": FBSDKAccessToken.currentAccessToken().tokenString
            ],
            encoding: ParameterEncoding.URL,
            headers: nil
            ).responseJSON { (response) -> Void in
                if response.result.error == nil {
                    completion?(success: response.result.isSuccess)
                } else {
                    print(response.result.error?.debugDescription)
                    completion?(success: false)
                }
        }
    }
    
}
