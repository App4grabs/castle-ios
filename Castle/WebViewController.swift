//
//  WebViewController.swift
//  Castle
//
//  Created by Gabriel Archer on 12/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit

@objc protocol WebViewControllerDelegate {
    optional func webViewController(webViewController: WebViewController, redirectURL: NSURL)
}

class WebViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    var url: NSURL?
    var content: String?
    var delegate: WebViewControllerDelegate?
    var activityIndicatorView: UIActivityIndicatorView?
    var shouldScaleToFit = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .White)
        activityIndicatorView?.hidesWhenStopped = true
        activityIndicatorView?.stopAnimating()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicatorView!)
        let menuButton = UIButton(frame: CGRectMake(0, 0, 48, 40))
        menuButton.setImage(UIImage(named: "back"), forState: .Normal)
        menuButton.contentHorizontalAlignment = .Left
        menuButton.addTarget(self, action: #selector(WebViewController.back), forControlEvents: .TouchUpInside)
        let leftButton = UIBarButtonItem(customView: menuButton)
        navigationItem.leftBarButtonItem = leftButton
        navigationItem.hidesBackButton = true

        webView.scalesPageToFit = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        webView?.delegate = self
        if (content != nil) {
            webView?.loadHTMLString(content!, baseURL: nil)
        } else if (url != nil) {
            let request = NSMutableURLRequest(URL: url!)
            webView?.loadRequest(request)
        }
        webView.scalesPageToFit = shouldScaleToFit
        (self.revealViewController() as! RevealViewController).shouldRotate = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        (self.revealViewController() as! RevealViewController).shouldRotate = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func back() {
        navigationController?.popViewControllerAnimated(true)
    }

    // MARK: - Delegates
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let url = request.URL {
            if let host = url.host {
                if host == "localhost" {
                    self.delegate?.webViewController?(self, redirectURL: request.URL!)
                    activityIndicatorView?.stopAnimating()
                    return false
                }
            }
        }
        return true
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        activityIndicatorView?.stopAnimating()
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        activityIndicatorView?.startAnimating()
    }
    
}
