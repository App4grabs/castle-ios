//
//  FacebookPost.swift
//  Castle
//
//  Created by Gabriel Archer on 12/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import RealmSwift

class FacebookPost: Object {
    
    dynamic var postId = ""
    dynamic var type = ""
    dynamic var message = ""
    dynamic var link = ""
    dynamic var picture = ""
    dynamic var createdTime = ""
    dynamic var fromId = ""
    dynamic var fromName = ""
    dynamic var fromPicture = ""
    dynamic var likes = 0
    dynamic var comments = 0
    dynamic var shares = 0
    dynamic var liked = false
 
    override class func primaryKey() -> String {
        return "postId"
    }

}
