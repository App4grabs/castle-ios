//
//  HomeViewController.swift
//  Castle
//
//  Created by Gabriel Archer on 06/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import MediaPlayer
import RTLabel

class HomeViewController: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var rtLabel: RTLabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    var moviePlayer: MPMoviePlayerController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        widthConstraint.constant = view.frame.width - 30
        rtLabel.frame = CGRectMake(
            rtLabel.frame.origin.x,
            rtLabel.frame.origin.y,
            view.frame.width - 30,
            rtLabel.frame.height
        )
        rtLabel.text = NSLocalizedString("HOME", comment: "Home text")
        rtLabel.font = UIFont.systemFontOfSize(15)
        rtLabel.textColor = UIColor.blackColor()
        let optimumSize = rtLabel.optimumSize
        widthConstraint.constant = optimumSize.width
        heightConstraint.constant = optimumSize.height
        scrollView.contentSize = optimumSize
        scrollView.setContentOffset(CGPointZero, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func playVideo(sender: UIButton) {
        if let videoURL = NSBundle.mainBundle().URLForResource("Castle", withExtension: "mp4") {
            print(videoURL)
            moviePlayer = MPMoviePlayerController(contentURL: videoURL)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.playCompleted(_:)),
                name: MPMoviePlayerWillExitFullscreenNotification, object: moviePlayer)
            moviePlayer?.prepareToPlay()
            self.view.addSubview(moviePlayer!.view)
            moviePlayer?.fullscreen = true
            moviePlayer?.play()
            (self.revealViewController() as! RevealViewController).shouldRotate = true
        }
    }

    func playCompleted(notification: NSNotification) {
        (self.revealViewController() as! RevealViewController).shouldRotate = false
        if let moviePlayer = notification.object as? MPMoviePlayerController {
            NSNotificationCenter.defaultCenter().removeObserver(self,
                name: MPMoviePlayerWillExitFullscreenNotification, object: moviePlayer)
            moviePlayer.view.removeFromSuperview()
        }
    }
    
}
