//
//  AppDelegate.swift
//  Castle
//
//  Created by Gabriel Archer on 06/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import Crittercism
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // DataManager.shared.clearData()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        Crittercism.enableWithAppID(Config.shared.crittercismId)

        let settings = UIUserNotificationSettings(forTypes: [.Sound, .Alert, .Badge], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)

        if launchOptions != nil {
            if launchOptions![UIApplicationLaunchOptionsRemoteNotificationKey] != nil {
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "Newsletter")
                NSUserDefaults.standardUserDefaults().synchronize()
            }
        }

        //Init AWS
        let credentialProvider = AWSStaticCredentialsProvider(accessKey: Config.shared.AWSKey, secretKey: Config.shared.AWSSecret)
        let configuration = AWSServiceConfiguration(region: .EUWest1, credentialsProvider: credentialProvider)
        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = configuration
        
        let field = UITextField()
        self.window?.addSubview(field)
        field.becomeFirstResponder()
        field.resignFirstResponder()
        field.removeFromSuperview()

        return true
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        application.registerForRemoteNotifications()
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let devToken = "\(deviceToken)".regReplace("[<> ]*", with: "")
        AWSManager.shared.registerEndpoint("\(devToken)") { (endpoint) -> Void in
            if endpoint != nil {
                print(endpoint)
            }
        }
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        NSNotificationCenter.defaultCenter().postNotificationName("DidReceiveRemoteNotification", object: nil)
        if let aps = userInfo["aps"] as? Dictionary<String, String> {
            APIManager.shared.alert("Newsletter", message: aps["alert"]!)
        }
    }

    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
        application.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

