//
//  CourseViewController.swift
//  Castle
//
//  Created by Gabriel Archer on 06/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit

class CourseViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var courses: Array<Array<Dictionary<String, String>>>?
    var images = ["book", "graduation", "people"]
    var titles = ["General English", "Exam Preparation", "Special Option"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let path = NSBundle.mainBundle().pathForResource("courses", ofType: "json") {
            if let jsonData = try? NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe) {
                if let jsonResult = try? NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as! Array<Array<Dictionary<String, String>>> {
                    self.courses = jsonResult
                }
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return courses?.count ?? 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (courses?[section].count ?? 0) + 1
    }
    
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let view = UIView(frame: CGRectMake(0, 0, tableView.frame.width, 40))
//        view.backgroundColor = UIColor.whiteColor()
//        let imageView = UIImageView(frame: CGRectMake(15, 5, 25, 25))
//        imageView.image = UIImage(named: images[section])
//        view.addSubview(imageView)
//        let label = UILabel(frame: CGRectMake(55, 5, tableView.frame.width - 70, 25))
//        label.text = titles[section] ?? ""
//        label.textColor = UIColor(hex: 0xe35105)
//        label.font = UIFont.boldSystemFontOfSize(18)
//        view.addSubview(label)
//        return view
//    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("HeaderCell", forIndexPath: indexPath) as! CourseHeaderCell
            cell.titleLabel.text = titles[indexPath.section] ?? ""
            cell.iconView.image = UIImage(named: "\(images[indexPath.section])White")
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
            cell.textLabel?.text = courses?[indexPath.section][indexPath.row - 1]["title"] ?? ""
            return cell
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let viewController = segue.destinationViewController as? DetailViewController {
            if let indexPath = tableView.indexPathForSelectedRow {
                viewController.course = courses?[indexPath.section][indexPath.row - 1]
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
            }
        }
    }

}

class CourseHeaderCell: UITableViewCell {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
}
