//
//  ViewController.swift
//  Castle
//
//  Created by Gabriel Archer on 08/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapViewController: BaseViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var transportSegmentedControl: UISegmentedControl!
    
    let locationManager = CLLocationManager()
    let position = CLLocationCoordinate2DMake(50.82416, -0.144687);
    var overlay: MKOverlay?
    var isDirection = false
    var transportType: MKDirectionsTransportType = .Automobile

    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self

        mapView.delegate = self
        mapView.centerCoordinate = position
        let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        mapView.region = MKCoordinateRegion(center: position, span: span)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = position
        annotation.title = "Castle School of English"
        annotation.subtitle = "12 and 41 Dyke Road, Brighton"
        mapView(mapView, viewForAnnotation: annotation)?.annotation = annotation
        mapView.addAnnotation(annotation)
        
        segmentedControl.layer.cornerRadius = 5
        transportSegmentedControl.layer.cornerRadius = 5

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Map View
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            locationManager.startUpdatingLocation()
            mapView.showsUserLocation = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        if !isDirection {
            return
        }
        if let location = locations.first {
            let sourcePlacemark = MKPlacemark(coordinate: location.coordinate, addressDictionary: nil)
            let destinationPlacemark = MKPlacemark(coordinate: position, addressDictionary: nil)
            
            let request: MKDirectionsRequest = MKDirectionsRequest()
            request.source = MKMapItem(placemark: sourcePlacemark)
            request.destination = MKMapItem(placemark: destinationPlacemark)
            request.requestsAlternateRoutes = true
            request.transportType = transportType
            let directions = MKDirections(request: request)
            directions.calculateDirectionsWithCompletionHandler({ (response, error) -> Void in
                if error != nil || response == nil {
                    //Alert
                    return
                }
                if let currentRoute = response!.routes.first {
                    self.mapView.setVisibleMapRect(currentRoute.polyline.boundingMapRect, animated: true)
                    if self.overlay != nil {
                        self.mapView.removeOverlay(self.overlay!)
                    }
                    self.overlay = currentRoute.polyline
                    self.mapView.addOverlay(self.overlay!)
                }
            })
            
        }
    }
    
    // MARK: - Map View
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {

        if (annotation is MKUserLocation) {
            return nil
        }
        
        let reuseId = "pin"
        
        var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            
            annotationView?.canShowCallout = true
            annotationView?.image = UIImage(named: "gps-pin")
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = UIColor.orangeColor()
            renderer.lineWidth = 4
            return renderer
        }
        return MKOverlayRenderer()
    }

    // MARK: - User Interactions
    
    @IBAction func changeMapType(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            mapView.mapType = .Standard
        } else {
            mapView.mapType = .Satellite
        }
    }
    
    @IBAction func changeTransportType(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            isDirection = false
            if self.overlay != nil {
                self.mapView.removeOverlay(self.overlay!)
                mapView.centerCoordinate = position
                let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
                mapView.region = MKCoordinateRegion(center: position, span: span)
            }
        } else if sender.selectedSegmentIndex == 1 {
            isDirection = true
            transportType = .Walking
            startDirection()
        } else {
            isDirection = true
            transportType = .Automobile
            startDirection()
        }

    }

    func startDirection() {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .NotDetermined {
                locationManager.requestWhenInUseAuthorization()
            } else if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
                locationManager.startUpdatingLocation()
            } else {
                //Let the user know that fucked
            }
        }
    }
    
}

