//
//  AWSManager.swift
//  Castle
//
//  Created by Gabriel Archer on 01/05/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import AWSSNS
import AWSCore

class AWSManager: NSObject {
    
    static var shared = AWSManager()

    func registerEndpoint(deviceToken: String, completion: (endpoint: String?) -> Void) {
        var endpoint: String?
        let sns = AWSSNS.defaultSNS()
        let request = AWSSNSCreatePlatformEndpointInput()
        request.token = deviceToken
        request.platformApplicationArn = Config.shared.SNSApp
        sns.createPlatformEndpoint(request).continueWithBlock { (task) -> AnyObject? in
            if task.error != nil {
                print(task.error!.localizedDescription)
                completion(endpoint: nil)
            } else {
                let response = task.result as! AWSSNSCreateEndpointResponse
                endpoint = response.endpointArn
                let request = AWSSNSSubscribeInput()
                request.endpoint = response.endpointArn
                request.protocols = "application"
                request.topicArn = Config.shared.SNSTopic
                sns.subscribe(request).continueWithBlock({ (task) -> AnyObject? in
                    completion(endpoint: endpoint)
                    return nil
                })
            }
            return nil
        }
    }

}
