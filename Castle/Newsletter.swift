//
//  Newsletter.swift
//  Castle
//
//  Created by Gabriel Archer on 01/05/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import RealmSwift

class Newsletter: Object {
    
    dynamic var letterId = 0
    dynamic var subject = ""
    dynamic var time = 0
    dynamic var message = ""
    
}
