//
//  MenuViewController.swift
//  Castle
//
//  Created by Gabriel Archer on 06/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import FBSDKShareKit

class MenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.revealViewController().rearViewRevealWidth = self.view.frame.width - 50
        // Do any additional setup after loading the view.
    }

    @IBAction func inviteFriends(sender: UIButton) {
        let content = FBSDKAppInviteContent()
        content.appLinkURL = NSURL(string: Config.shared.FacebookInviteLink)
        content.appInvitePreviewImageURL = NSURL(string: Config.shared.FacebookImageLink)
        FBSDKAppInviteDialog.showFromViewController(self, withContent: content, delegate: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
