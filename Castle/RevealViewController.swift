//
//  ViewController.swift
//  Castle
//
//  Created by Gabriel Archer on 06/03/2016.
//  Copyright © 2016 Gabriel Archer. All rights reserved.
//

import UIKit
import MediaPlayer
import SWRevealViewController

class RevealViewController: SWRevealViewController {

    var shouldRotate = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func shouldAutorotate() -> Bool {
        return shouldRotate
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if shouldRotate {
            return UIInterfaceOrientationMask.AllButUpsideDown
        } else {
            return UIInterfaceOrientationMask.Portrait
        }
    }
    

}

